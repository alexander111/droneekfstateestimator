// Jesus' to-do list:
// TODO: Cosas que me gustaría añadir al EKF,
//			- TODO: Hacer pruebas con diferentes constantes para las varianzas de medida, de ruido a la entrada, etc
//			- TODO: habilitar/permitir cambios de configuracion del EKF online

#ifndef DRONEEKFSTATEESTIMATOR_H
#define DRONEEKFSTATEESTIMATOR_H

#include "extendedKalmanFilter.h"
#include "matrixLib.h"

///// Model definition //////
#define _MULTIROTOR_IS_PARROT_  //#include "config_Mydrone.h"

#ifdef _MULTIROTOR_IS_PARROT_
//#include "models/EKF_config_Parrot.h"
#include "models/EKF_model_Parrot.h"
typedef ParrotModel1 Multirotor_Model;
#endif

//Other includes
#include <math.h>
#include "cvg_utils_library.h"      // from lib_cvgutils package
#include "referenceFrames.h"    // from referenceFrames package
#include "Timer.h"

class DroneEKFStateEstimator {
    // Constructors, Destructor and related functions
public:
    DroneEKFStateEstimator(int idDrone, const std::string &stackPath_in);
    ~DroneEKFStateEstimator();

    // reset and start functions
public:
    bool resetStateEstimator();
    bool startStateEstimator();

    // EKF related internal variables
private:
    Multirotor_Model multirotorModel;
    // Correspondence between commands [-1,1] to real values of pitch, roll, etc (depends on autopilot config)
    inline void setModelIntputGains(double gain_pitch, double gain_roll, double gain_dyaw, double gain_dz);

    // ***** Internal objects and variables *****
    int numStates, numInputs, numMeasures;
    ContinuousExtendedKalmanFilter 	EKF;
    Timer                           timer;

    // State Estimator configuration, TODO: Delete these variables if they are unnecesary
    CVG::Vector VarActuation_EKF;
    CVG::Vector VarStateModel_EKF;
    CVG::Matrix MatPInit;
    CVG::Vector VarObservation_EKF;

    // Inputs to State Estimator
    CVG::Vector RealActuation;
    CVG::Vector RealObservation;
    double maximum_ground_optical_flow_measurement;
    CVG::Vector FlagObservers;

    // Ouput of the State Estimator
    CVG::Vector EstimatedState;
    CVG::Matrix MatVarEstimatedState;
    CVG::Vector EstimatedObservation;            // ROSdatatype droneMsgs::dronePose, droneMsgs::droneSpeeds
    CVG::Matrix MatVarOutput;
    // END: ***** Internal objects and variables *****

public:
    // State estimation
    int stateEstimation(float mahalanobisDistance);

    // Set measurements
    int setDroneMeasurementRotationAngles(float roll, float pitch, float yaw);                          // ROSdatatype sensor_msgs::Imu
    int setDroneMeasurementGroundOpticalFlow(float vx, float vy);    // ROSdatatype ??::??
    int setDroneMeasurementAltitude(float altitude);                // ROSdatatype ??::??

    // Set drone inputs
public:
    int setDroneCommands(float pitch, float roll, float dyaw, float dalt);
private:
    void setDroneCommandPitch(float pitch);
    void setDroneCommandRoll( float roll);
    void setDroneCommandDYaw( float dyaw);
    void setDroneCommandDAlt( float dalt);

    //Get drone estimated state
public:
    void getDroneEstimatedPose_LMrTwrtEKF(float *x, float *y, float *z, float *yaw, float *pitch, float *roll);
    void getDroneEstimatedPose_GMRwrtGFF(float *x, float *y, float *z, float *yaw, float *pitch, float *roll);
    void getDroneEstimatedSpeed_LMrTwrtEKF(float *dx, float *dy, float *dz, float *dyaw, float *dpitch, float *droll);
    void getDroneEstimatedSpeed_GMRwrtGFF(float *dx, float *dy, float *dz, float *dyaw, float *dpitch, float *droll);
    void getEstimatedObservation(CVG::Vector *estObserv);
    // TODO: Get variances of the pose and speeds estimation

    // Transformation between different reference frames, see documentation for more information
public:
    void setInitDroneYaw(double init_telemetry_yaw);
    // Homogeneous tranforms
private:
    cv::Mat matHomog_drone_GMR_t0_wrt_GFF;          // from droneXX.xml configFile
    cv::Mat matHomog_drone_LMrT_wrt_drone_GMR;      // constant, see reference frames' definition (see pdf)
    cv::Mat matHomog_drone_GMR_wrt_drone_LMrT;      // constant, see reference frames' definition (see pdf)
    cv::Mat matHomog_EKF_LMrT_wrt_drone_LMrT_t0;    // from initial yaw
    cv::Mat matHomog_EKF_LMrT_wrt_GFF;              // to obtain SLAMs required transform from EKF data
    cv::Mat matHomog_droneLMrT_wrt_EKF_LMrT;        // from EKF
    cv::Mat matHomog_drone_GMR_wrt_GFF;             // for SLAM
    cv::Mat matRotation_EKF_LMrT_wrt_GFF;           // to obtain droneEstimatedSpeed_GMRwrtGFF
    cv::Mat matRotation_EKF_LMrT_wrt_GFF_only_yaw;           // to obtain droneEstimatedSpeed_GMRwrtGFF
    cv::Mat vecSpeed_drone_LMrT_wrt_EKF_LMrT;       // to obtain droneEstimatedSpeed_GMRwrtGFF
    cv::Mat vecSpeed_drone_LMrT_wrt_GFF;            // to obtain droneEstimatedSpeed_GMRwrtGFF
};

#endif /* DRONEEKFSTATEESTIMATOR_H */
